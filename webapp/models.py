# webapp/models.py
from webapp import db


class Subscriber(db.Model):
    """Data model for newsletter subscribers"""

    __tablename__ = 'subscribers'
    id = db.Column(
        db.Integer,
        primary_key=True
    )
    name = db.Column(
        db.String(64),
        index=False,
        unique=True,
        nullable=False
    )
    created = db.Column(
        db.DateTime,
        index=False,
        unique=False,
        nullable=False
    )

    def __repr__(self):
        return '<User {}>'.format(self.name)


class Webevent(db.Model):
    """Data model for webevents"""

    __tablename__ = 'webevents'
    id = db.Column(
        db.Integer,
        primary_key=True
    )
    sname = db.Column(
        db.String(64),
        index=False,
        unique=True,
        nullable=False
    )
    description = db.Column(
        db.Text,
        index=False,
        unique=False,
        nullable=True
    )
    planned = db.Column(
        db.DateTime,
        index=False,
        unique=False,
        nullable=False
    )

    def __repr__(self):
        return '<Event {} {}>'.format(self.sname, self.id)
